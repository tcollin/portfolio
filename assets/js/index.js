import _ from 'lodash';
import css from '../css/style.css';
import './locomotive';
import './brands';
import './fontawesome';
import './solid';

function component() {
  const element = document.createElement('div');

   element.innerHTML = _.join(['Hello', 'webpack'], ' ');

  return element;
}

document.body.appendChild(component());