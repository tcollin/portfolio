<?php

require '../vendor/autoload.php';


$loader = new \Twig\Loader\FilesystemLoader('../templates');
$twig = new \Twig\Environment($loader);

$uri = $_SERVER['REQUEST_URI'];
$router = new AltoRouter();

define('BASE_URL', dirname($_SERVER['SCRIPT_NAME']));

$router->map('GET', '/', 'page');

$match = $router->match();

if (!is_array($match))
    echo $twig->render('errors/404.html.twig');

if (is_array($match)) {
    $params = $match['params'];
    echo $twig->render($match['target'].'.html.twig');
}
