const path = require('path');

module.exports = {
  mode: 'development',
  entry: './assets/js/index.js',
  cache: false,
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
